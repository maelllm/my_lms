from django.contrib import admin

from core.models import Profile, Customer

admin.site.register([Profile, Customer])

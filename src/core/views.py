from django.contrib.auth import get_user_model, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, FormView, RedirectView, UpdateView
from django.http import HttpResponse

from core.forms import RegistrationForm, EmailForm, ProfileForm
from core.services.emails import send_registration_email
from core.services.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ['get']


class PageNotFoundView(TemplateView):
    template_name = "404.html"


class Login(LoginView):
    pass


class Logout(LogoutView):
    pass


class Registration(CreateView):
    form_class = RegistrationForm
    template_name = "registration/create_user.html"
    success_url = reverse_lazy("core:index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(
            request=self.request, user_instance=self.object
        )

        return super().form_valid(form)


class EmailSend(LoginRequiredMixin, FormView):
    form_class = EmailForm
    template_name = "email_sending.html"
    success_url = reverse_lazy("core:index")
    login_url = "core:login"

    def form_valid(self, form):
        form.test_send()
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("core:index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Try again")
        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user, backend='django.contrib.auth.backends.ModelBackend')
            return super().get(request, *args, **kwargs)

        return HttpResponse("Done")


class ProfileView(LoginRequiredMixin, UpdateView):
    form_class = ProfileForm
    template_name = "user_profile.html"
    success_url = reverse_lazy("core:user_profile")

    def get_object(self, queryset=None):
        return self.request.user.profile

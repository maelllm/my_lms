from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from core.views import IndexView, Login, Logout, Registration, EmailSend, ActivateUser, ProfileView

app_name = "core"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="registration"),
    path("send-email/", EmailSend.as_view(), name="send-email"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path("profile/", ProfileView.as_view(), name="user_profile")

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _

from core.managers import CustomerManager


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("first_name"), max_length=150, blank=True)
    last_name = models.CharField(_("last_name"), max_length=150, blank=True)
    phone_number = PhoneNumberField(_("phone number"), null=True, blank=True)
    email = models.EmailField(_("email address"), null=True, blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = CustomerManager()

    USERNAME_FIELD = "phone_number"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return str(self.phone_number)

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_working_time(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class Profile(models.Model):
    STUDENT = "ST"
    TEACHER = "TH"
    MENTOR = "MT"
    USER_TYPE_CHOICE = [
        (STUDENT, "Student"),
        (TEACHER, "Teacher"),
        (MENTOR, "Mentor"),
    ]
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    photo = models.ImageField(upload_to="media", null=True)
    location = models.CharField(max_length=150)
    birth_date = models.DateField(blank=True)
    height = models.PositiveSmallIntegerField()
    race = models.CharField(max_length=10, default="white")

    user_type = models.CharField(max_length=2, choices=USER_TYPE_CHOICE)

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name} {self.user.email}"

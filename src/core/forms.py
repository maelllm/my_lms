import datetime

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.mail import send_mail
from django.forms import ModelForm

from config.settings import EMAIL_HOST_USER
from core.models import Profile


class RegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("phone_number", "email", "password1", "password2")


class EmailForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField(required=True)
    message = forms.CharField(required=True)
    subject = forms.CharField(required=True)

    def get_info(self):
        cl_data = super().clean()

        name = cl_data.get('name')
        email = [cl_data.get('email')]
        subject = cl_data.get('subject')
        message = f'Dear {name}, \n{cl_data.get("message")} {str(datetime.datetime.now())}'
        return subject, message, email

    def test_send(self):
        subject, message, email = self.get_info()

        send_mail(
            subject=subject,
            message=message,
            from_email=EMAIL_HOST_USER,
            recipient_list=email
        )


class ProfileForm(ModelForm):
    photo = forms.ImageField(widget=forms.FileInput(attrs={"class": "form-control-file"}))

    class Meta:
        model = Profile
        fields = ["photo", "location", "birth_date", "height", "race"]

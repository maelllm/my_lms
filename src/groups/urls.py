from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from groups.views import GetAllGroupsView, CreateGroupView, UpdateGroupView, DeleteGroupView

app_name = "groups"

urlpatterns = [
    path("", GetAllGroupsView.as_view(), name="all_groups"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<int:pk>/", DeleteGroupView.as_view(), name="delete_group"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.db import models
from faker import Faker
from faker_music import MusicProvider


class Group(models.Model):
    url = models.URLField(max_length=100, null=True)
    creation_date = models.DateTimeField(null=True)
    group_job_name = models.CharField(max_length=50)
    group_color = models.CharField(max_length=20)
    favorite_genre = models.CharField(max_length=40)
    card_number = models.IntegerField(null=True)

    class Meta:
        verbose_name = "Groups"
        verbose_name_plural = "All Groups"
        ordering = ["group_color"]

    def __str__(self):
        return f"{self.group_job_name}"

    @classmethod
    def generate_groups(cls, amount):
        faker = Faker('UK')
        faker_2 = Faker()
        faker_2.add_provider(MusicProvider)

        for _ in range(amount):
            cls.objects.create(
                url=faker_2.url(),
                creation_date=faker.date_time_between(
                    start_date='-15y', end_date='-1y'),
                group_job_name=faker.job(),
                group_color=faker.color_name(),
                favorite_genre=faker_2.music_genre(),
                card_number=faker_2.credit_card_number(),
            )

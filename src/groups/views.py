from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView, CreateView, ListView
from .models import Group
from .forms import GroupForm


class GetAllGroupsView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups_list.html"
    queryset = Group.objects.all()
    context_object_name = "groups"
    login_url = "core:login"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        query = self.request.GET
        if query:
            search_fields = ["creation_date", "url", "group_job_name",
                             "group_color", "favorite_genre", "card_number"]
            for parameter_name, parameter_value in query.items():
                if parameter_value:
                    if parameter_name == "search_text":
                        or_filter = Q()
                        for field in search_fields:
                            or_filter |= Q(**{f"{field}__icontains": parameter_value})
                        queryset = Group.objects.filter(or_filter)
                    else:
                        queryset = Group.objects.filter(**{parameter_name: parameter_value})
        return queryset


class CreateGroupView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    template_name = "object_create.html"
    success_url = reverse_lazy("groups:all_groups")
    login_url = "core:login"


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    template_name = "object_update.html"
    success_url = reverse_lazy("groups:all_groups")
    login_url = "core:login"


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy("groups:all_groups")
    template_name = "confirm_delete.html"
    login_url = "core:login"

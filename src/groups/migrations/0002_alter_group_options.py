# Generated by Django 4.1 on 2022-11-07 17:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="group",
            options={
                "ordering": ["group_color"],
                "verbose_name": "Groups",
                "verbose_name_plural": "All Groups",
            },
        ),
    ]

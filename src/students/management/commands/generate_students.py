from django.core.management.base import BaseCommand
from faker import Faker
from pprint import pprint


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(nargs="+", type=int, dest="args")

    def handle(self, *args, **kwargs):
        student_info = Faker('UK')
        student_data = []
        for i in range(*args):
            student_data.append(student_info.first_name())

        pprint(student_data)

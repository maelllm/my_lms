from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from .models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    fields = (("first_name", "last_name"), "email", "grade")
    list_display = ("first_name", "last_name", "email", "group_name", "grade", "age", "links_to_groups")
    list_filter = ["grade"]
    list_editable = ["grade"]
    search_fields = ("email", "first_name", "last_name")
    empty_value_display = '-empty-'

    def group_name(self, obj):
        return obj.group.favorite_genre

    def links_to_groups(self, obj):
        if obj.group:
            return format_html(
                f"<a class='button',"
                f" href='{reverse('admin:groups_group_change', args=[obj.group.pk])}'>{obj.group.group_job_name}</a>")
        return "Group is not assigned"

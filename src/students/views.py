from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from .forms import StudentForm
from .models import Student


class GetAllStudentsView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students_list.html"
    queryset = Student.objects.all()
    context_object_name = "students"
    login_url = "core:login"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        query = self.request.GET
        print(query)

        if query:
            search_fields = ["first_name", "last_name", "email"]
            for parameter_name, parameter_value in query.items():
                if parameter_value:
                    if parameter_name == "search_text":
                        or_filter = Q()
                        for field in search_fields:
                            or_filter |= Q(**{f"{field}__icontains": parameter_value})
                        queryset = Student.objects.filter(or_filter)
                    else:
                        queryset = Student.objects.filter(**{parameter_name: parameter_value})
        return queryset


class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentForm
    template_name = "object_create.html"
    success_url = reverse_lazy("students:all_students")
    login_url = "core:login"


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "object_update.html"
    success_url = reverse_lazy("students:all_students")
    login_url = "core:login"


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy("students:all_students")
    template_name = "confirm_delete.html"
    login_url = "core:login"

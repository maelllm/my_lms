from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from students.views import GetAllStudentsView, CreateStudentView, UpdateStudentView, DeleteStudentView

app_name = "students"

urlpatterns = [
    path("", GetAllStudentsView.as_view(), name="all_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<int:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<int:pk>/", DeleteStudentView.as_view(), name="delete_student"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

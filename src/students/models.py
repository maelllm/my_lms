from datetime import datetime

from django.core.validators import MinLengthValidator, FileExtensionValidator
from django.db import models
from faker import Faker

from students.utils import first_name_validator


class Student(models.Model):
    first_name = models.CharField(
        max_length=100,
        null=True,
        validators=[MinLengthValidator(2), first_name_validator],
    )
    last_name = models.CharField(
        max_length=100,
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, null=True)
    birth_date = models.DateField(null=True)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    group = models.ForeignKey(
        to="groups.Group", on_delete=models.CASCADE, default=1, related_name="student_group",
    )
    avatar = models.ImageField(upload_to="media", null=True, default="media/student_avatar.jpeg")

    resume = models.FileField(null=True, upload_to="media",
                              validators=[FileExtensionValidator(allowed_extensions=["pdf"])])

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "All Students"
        ordering = ["last_name"]

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def age(self):
        if datetime.now().month < self.birth_date.month:
            return datetime.now().year - self.birth_date.year - 1
        if datetime.now().month == self.birth_date.month:
            if datetime.now().day < self.birth_date.day:
                return datetime.now().year - self.birth_date.year - 1
        return datetime.now().year - self.birth_date.year

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )

from django.db import models
from faker import Faker


class Teacher(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    address = models.CharField(max_length=150)
    ssn = models.IntegerField(null=True)
    phone_number = models.CharField(max_length=25, null=True)
    birth_date = models.DateTimeField(null=True)
    group = models.ManyToManyField(
        to="groups.Group",
        related_name="teacher_group",
    )
    student = models.ManyToManyField(
        to="students.Student",
        related_name="students",
    )
    avatar = models.ImageField(upload_to="media", null=True, default="media/teacher_avatar.jpg")

    class Meta:
        verbose_name = "Teachers"
        verbose_name_plural = "All Teachers"
        ordering = ["last_name", "first_name"]

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.address} {self.ssn} " \
               f"{self.phone_number}"

    @classmethod
    def generate_teachers(cls, amount):
        faker = Faker('UK')

        for i in range(amount):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                address=faker.address(),
                ssn=faker.ssn(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_of_birth(minimum_age=27, maximum_age=70)
            )

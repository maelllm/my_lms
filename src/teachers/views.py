from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

from .models import Teacher
from .forms import TeacherForm


class GetAllTeachersView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers_list.html"
    queryset = Teacher.objects.prefetch_related("group").all()
    context_object_name = "teachers"
    login_url = "core:login"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        query = self.request.GET
        if query:
            search_fields = ["first_name", "last_name", "email", "address", "ssn", "phone_number",
                             "birth_date"]
            for parameter_name, parameter_value in query.items():
                if parameter_value:
                    if parameter_name == "search_text":
                        or_filter = Q()
                        for field in search_fields:
                            or_filter |= Q(**{f"{field}__icontains": parameter_value})
                        queryset = Teacher.objects.filter(or_filter)
                    else:
                        queryset = Teacher.objects.filter(**{parameter_name: parameter_value})
        return queryset


class CreateTeacherView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "object_create.html"
    success_url = reverse_lazy("teachers:all_teachers")
    login_url = "core:login"


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "object_update.html"
    success_url = reverse_lazy("teachers:all_teachers")
    login_url = "core:login"


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy("teachers:all_teachers")
    template_name = "confirm_delete.html"
    login_url = "core:login"

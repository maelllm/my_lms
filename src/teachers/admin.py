from django.contrib import admin
from datetime import datetime

from django.urls import reverse
from django.utils.html import format_html

from .models import Teacher


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            "Personal info", {
                'fields': (("first_name", "last_name"), ("email", "phone_number"))
            },
        ),
        (
            "Advanced", {
                "classes": ("collapse",),
                "fields": (("ssn", "birth_date"), "group", "avatar", "student")
            }
        )
    )

    list_display = ("last_name", "first_name_red", "birth_date", "phone_number", "students_count",
                    "age", "groups_list")
    list_display_links = ("last_name", "first_name_red")
    list_filter = ("student", "group")
    list_editable = ["phone_number"]
    date_hierarchy = "birth_date"
    search_fields = ("email", "first_name", "last_name__istartwith")
    empty_value_display = '-empty-'

    def first_name_red(self, obj):
        return format_html(
            f"<span style='color:#F71E0C;''>{obj.first_name}</span>"
        )

    def students_count(self, obj):
        return obj.student.count()

    @admin.display(empty_value="Eternal")
    def age(self, obj):
        if datetime.now().month < obj.birth_date.month:
            return datetime.now().year - obj.birth_date.year - 1
        if datetime.now().month == obj.birth_date.month:
            if datetime.now().day < obj.birth_date.day:
                return datetime.now().year - obj.birth_date.year - 1
        return datetime.now().year - obj.birth_date.year

    def groups_list(self, obj):
        if obj.group:
            groups = obj.group.all()
            links = []
            for group in groups:
                links.append(
                    f"<a class='button',"
                    f" href='{reverse('admin:groups_group_change', args=[group.pk])}'>{group.group_job_name}</a>")
            return format_html("</br>".join(links))
        return "Group is not yet assigned"

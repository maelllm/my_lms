from django.conf import settings
from django.conf.urls.static import static
from django.urls import path


from teachers.views import GetAllTeachersView, CreateTeacherView, UpdateTeacherView, DeleteTeacherView

app_name = "teachers"

urlpatterns = [
    path("", GetAllTeachersView.as_view(), name="all_teachers"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<int:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
